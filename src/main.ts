import "./style.css";

const app = document.querySelector<HTMLDivElement>("#app")!;

const a: string = "Hey Oh";
const n: number = 41;
const b: boolean = true;
const d: null = null;

// array type
// ERROR // const arr: string[] = ["hug", "ugh", "hgu", 5]; // error msg: {"message": "Type 'number' is not assignable to type 'string'.",}
const arrStringNumber: any[] = ["hug", "ugh", "hgu", 5]; // it work but we lose all types benefices
const arr: string[] = ["hug", "ugh", "hgu"]; // OK, no error
// type an object, put the key in {} & we passed the value
const user: { firstname: string; lastname: string } = {
  firstname: "Neil",
  lastname: "Young",
};

const tech: { one: string; two: string } = {
  one: "TypeScript",
  two: "JavaScript",
};
console.log(user.firstname + " " + user.lastname);
console.log(`This is ${user.firstname} ${user.lastname}`);

/**
 * Narrowing exemple
 * typeOf / conditions
 */
function printId(id: string | number) {
  if (typeof id === "number") {
    console.log((id + 18).toString()); // type number
  } else {
    console.log(id.toUpperCase()); // type string
  }
}

console.log(printId("Number Of ID: ")); // 'id === string' so 'id.toUpperCase()'
/**
 * a is a string (only 2 communs types)
 */
function communTypes(a: string | boolean, b: string | number) {
  if (a === b) {
    a; // understand type string, the only commun type
  }
}
/**
 * with the same function but boolean more type
 * a is a string (only 2 communs types)
 * completion on a understand type string or boolean
 */
function communTypesTwo(a: string | boolean, b: string | number | boolean) {
  if (a === b) {
    a; // understand type string or boolean
  }
}

/**
 * Narrowing with inctanceOf
 * here we know that a is a Date type
 */
function narrowExemple(a: string | Date) {
  if (a instanceof Date) a; // know that a is a Date type
}

/**
 *
 * @param obj
 */

function getFavouriteNumber(): number {
  return 99;
}

//anonyme function : is syntax error in function name toUpperCase(), typescript  alert problem
// typage contrextuel, typescript

//utilise le typage de la fonction
//forEach pour deduire le type de s
const names = ["Gju", "Zry", "Uhtr"];
names.forEach(function (s) {
  console.log(s.toUpperCase());
});

names.forEach((t) => {
  console.log(t.toLowerCase());
});

// OBJET TYPE
function printCoord(pt: { x: number; y: number }) {
  console.log("geographic x point is " + pt.x);
  console.log("y is " + pt.y);
  console.log(`the x coordonates's geographicales values is ${pt.x}`);
}
printCoord({ x: 88, y: 231 });
/**
 *
 * @param obj
 * undefined
 */
// specifie not undefined value0
function printName(obj: { first: string; last?: string }) {
  printName({ first: "TypeScript" });
  printName({ first: "TypeScript", last: "JavaScript" });

  if (obj.last !== undefined) {
    console.log("hey " + obj.last.toUpperCase());
  }
  console.log(obj.last?.toUpperCase());
}

/**
 * union type definition
 */
function printIdTwo(id: number | string) {
  console.log("your id is: " + id);
}

/**
 * in operator
 */
type Dog = { wouf: () => void };
type Cat = { miaou: () => void };

function sound(animal: Dog | Cat) {
  if ("wouf" in animal) {
    console.log(animal.wouf);
  } else {
    console.log(animal.miaou);
  }
}
// reiteration properties
type Fish = { swim: () => void };
type Bird = { fly: () => void };
type Human = { swim?: () => void; fly?: () => void };

function move(animal: Fish | Bird | Human) {
  if ("swim" in animal) {
    console.log(animal);
  } else {
    console.log(animal);
  }
}

/**
 * Alias & Generics
 */
type Artist = { firtname: string; lastname: string; age: number; bith?: Date };
type Id = string | number;

const artist: Artist = { firtname: "Neil", lastname: "Young", age: 82 };
console.log(artist); // w'? ' make optionnal 'birth' Date parameter, otherwise, we we got an error: 'Date type is missing'

/************* Why Use Gernerics, a really Great TS notion ? **************
this fist function get an issu exemple that Gerneric can fix 
**************************************************************************/
/**
 * In this first function we don't Use Gernerics, and a big problem is that
 * we lost the type: `number` became 'any'
 * @param arg
 */
function identityLost(arg: any): any {
  return arg;
}
const aaa = identityLost(56); // WARNING, the is a type problem:
//`aaa` variable is not `number` type but `any` type !!!
// ! Here wer lost `number` type on `aa` to any type, Here we lost the type: `number` became 'any'

/******************************************************************************************************
 * ************ Use Gernerics, a really Great TS notion, It make TypeScript so Great !!! **************
 *******************************************************************************************************/
/**
 * In this second function we Use Gernerics, and fix problem of sort lost type
 * We don't wana lost a type like `number`, and certainly no to'any' (! 'any) is the type to never use, expect we don't have the choice or ??? )
 * To fix this problem, we want that function give the same type in enter & on return
 * so, we can pass to the identity funtion a parameter type as <PascalCase>
 * TypeScript convention is to name this function parapeter with the PascalCase Convention
 * @param arg
 */
function identity<ArgType>(arg: ArgType): ArgType {
  return arg;
}
const aa = identity<number>(56); // To fix the loosing type we has in our first variable `aaa`, we want that function give the same type in enter & on return
// so, we can pass to the identity funtion a parameter type in <> & use `PascalCase` into

// literals types with gerenics
const aaLiteral = identity(99); // aaLiteral type is '99', it's a literal type

/********* OTHER EXEMPLE WE CAN USE GENERIC ************/
/**
 * 'first' function have 'any' in param, array & return type: 'any'
 * but 'any' type is not a good practice
 */
// function first(arg: any[]): any {}

/********* SO WE CAN USE GENERIC LIKE THIS *************/

/**
 * Generics on an array
 * 'first' function have 'Type' in param, array & return type: 'Type'
 */
function first<Type>(arg: Type[]): Type {
  return arg[0];
}
// Now, we can know the type
const bb = first(["aze", "eza", "zae", 99]); // here, variable type is 'string | numbre'
const bbb = first(["aze", "eza", "zae"]); // here, variable type is 'string'

/***************** GENERIC works on NATIVE FUNCTIONS ****************/
/**
 * on NATIVE FUNCTIONS: HTMLButtonElement
 * adding <HTMLButtonElement>
 * add 'HTMLButtonElement | null' type
 */
const compteur = document.querySelector<HTMLButtonElement>("#compteur");

/**
 * on NATIVE FUNCTIONS: ARRAY
 */
const arrayFirst = ["aze", "eza", "zae"];
const natFunArray: Array<string | number> = ["aze", "eza", "zae", 99];

/**
 * we can get a 'Type' with a 'generic type'
 */
type Identity<ArgType> = (arg: ArgType) => ArgType;

/*********** FORCING A TYPE WITH A CONSTRAINT **********/
// contraint is that length must be a numer of an array
/**
 * error on consoleSize function
 * return a 'any' type
 */
// function consoleSize(arg) {
//   console.log(arg.length);
//   return arg;
// }
/**
 * another error on consoleSize:
 * Property 'length' does not exist on type 'Type'
 */
// function consoleSize<Type>(arg: Type): Type {
//   console.log(arg.length);
//   return arg;
// }
/**
 * we can extends on an object with a key: lenght type: number
 * when we will used this function, we ll must give an array with his number
 * this is the constraint
 */
function consoleSize<Type extends { length: number }>(arg: Type): Type {
  console.log(arg.length);
  return arg;
}

// const azz = consoleSize(3); // ERROR : constraint not respected
const azz = consoleSize(["3", 2]); // OKAY, Constraint respected

/************* a type can depend from anoter one.*******************/
type P = keyof Artist; // 'P' take all types of 'Artist'
/************* a type can herite from anoter one.*******************/
type ArtisteName = Artist["firtname"];

/************final resum generic TS ***********/
const input = document.querySelector("input"); // TS allow to know that 'input' is HTMLInputElement type

/*********** Les CLASSES ******************/
/**
 * exemple with reverse function that change array order that can cause troubles
 * we can add 'readonly' on a property
 * this property can't be changed
 * so, push or reverse are not allow
 *
 * we can do it on the return TYPE, or on all TYPES
 */
// function reverse<T>(arr: readonly T[]): T[] {
//   return arr.reverse();
// }

// we can use a spead operator to create a new array

//  ON FUTURE JS VERSION, WE WILL PUT A # on PRIVATE instancesz

function reverse<T>(arr: readonly T[]): T[] {
  return [...arr].reverse();
}

class A {
  public x = 3;
  private a = 30; // see POO encapsulation
  #aa = 30; // in futur ECMASCRIPT, # is like private

  log() {
    console.log(this.a); // ok, accessible because inside Classe A
  }

  log2() {
    console.log(this.#aa); // ok, accessible because inside Classe A
  }
}

const xInstance = new A();
const aInstance = new A();

console.log(xInstance.x); // ok, accessible
// console.log(aInstance.a); // error no accessible outside class instance

aInstance.log(); // ok , private, we can accessible inside Classe

class B {
  protected y = 857; // only children can access
}

class C extends B {
  log() {
    console.log(this.y); // Ok can acces because a is protected
  }
}

/**
 * ENCAPTULATION IS ONLY ON TS, NOT ON JS
 */

// console.log();

/**
 * Generate visibility by contructor with TYPE
 */
class D {
  constructor(public z: number) {}
}
const dInstance = new D(56);

console.log(dInstance.z); // ok, accessible with TYPE

/**
 * Generic Class & Contructor for TYPE
 *
 * this make reference on instance in POO
 */
class Collection<T> {
  constructor(private items: T[]) {}

  add(item: T): this {
    // Type return this, so the same istence is send
    this.items.push(item);
    return this; // ok, no error
  }
  first() {
    return this.items[0] || null; // reference on instance
  }
}

// create a new Collection, specifie comtent inside & initialise with good type
// const h = new Collection<number>([1, 2, 3, 4, 5, 6, 5, 4, 3, 2, 1, 0]);

// const h = new Collection([1, "2", 3, 4, 5, 6, 5, 4, 3, 2, 1, 0]);

const h = new Collection([1, "2", 3, 4, 5, 6, 5, 4, 3, 2, 1, 0]);
const i = h.add(300); // allways be a Collection type <number> but if I add a String in the new Collection array, type will change to TYPES: Collection<string | number>
const g = h.first();
// TS GIVE US THE TYPES OF VARIABLES LIKE
// const h: Collection<number>
// const g: number | null

/**
 * WE CAN CHANGE 'THIS' CONTEXT
 *
 * WARNING, IN FINAL COMPILE CODE, THIS CONTEXT CAN NO WORK
 *
 *CHANGE TO AN ARROW FUNCTION (see Subscriber2 function)
 */
class Subscriber {
  // put 'this' in function param
  //(this: HTMLInputElement, name: string, cb: Function) {
  // NOW every time I CALL 'THIS, TYPE ll reference to 'HTMLInputElement', GREAT !!!
  // this.
  //}
}

class Subscriber2 {
  // CHANGE TO AN ARROW FUNCTION to KEEP CODE When it will be compile in JS
  //on = (name: string, cb: Function) => {
  // NOW every time I CALL 'THIS, TYPE ll reference to 'HTMLInputElement', GREAT !!!
  // this.
  //};
}
/************************************
 * *********************************
 * ********************************
 */
app.innerHTML = `
  <h1> Why I like using ${tech.one} on my ${tech.two}'s applications!</h1>
  <p>
        I have learn progammation on JAVA, a very stong type langage with orientied object concept.
        A few years later, I was brought to design and develop a software factory named "FROGGIT". 
        To achieve my goals, coding in JavaScript was the best solution. I was taken by passion for this dynamic language, but I still had a very object-oriented vision, and I wasted a lot of time debugging and working on the organization of my code. 
        I found in TypeScript a way to make my JavaScript applications more robust, write robust and generic code... 
  </p>
  <ul>TypeScript Post French Traduction:
    <li>abtsract</li> 
    <li>presentation</li>
  </ul>
 
  <p>I have learn programmation on Java, an Oriented Object language with strong types.
  Then I have learn ${tech.one}, to make Front and BackEnd programmation, design & developing some Single Page Applications, Mobiles App CrossPlatformes App or Dresktop application (with Electron.js).
  My only problem with this great fonctionnal langage (as Elixir) is are that the types are very less, and Oriented Oject concepts advantages are not offocially reconnysed.
  And, here, I have discovered ${tech.one}, that respond to all off that concepts, types, Classes ...
  </p>
  <p>
  See my TypeScript application in Continuous Deloyment here : 
  <a href="https://typescript-formation.vercel.app/">TS app in CI/CD</a>
  </p>
  <ul>${tech.one} advantages are : 
    <li>a better auto-completion by code editor </li>
    <li>error are directly detected, even before save work</li>
    <li>is a Oriented Object language, like Java, with strong types </li>
        <li>get Oriented Object concepts notion, Class, methodes that can be public, private ou protected, or can extends others ...</li>
        <li>${tech.one} is inteliggi </li>
        <li>
          <p>
          See my TypeScript application in Continuous Deloyment here : 
          <a>https://typescript-formation.vercel.app/</a>
          </p>
        </li>
  </ul>

  <h2>Alias & Generics</h2
  <ul>
  <li>Alias</li>
  <li>Generics</li>
  <ul>

  <p>This TypeScript application, that use Vite.js bundle, with a CI/CD on GitLab & Vercel serve to learn advanced notions, test and progress ...</p>
  <a href="https://gitlab.com/HGouttebroze/typescript-formation" target="_blank">TypeScript Code Source on GitLab</a>

  <pre>
    <code>
      function sum(a: number, b?: number, c?: number): number {

    let result: number = a;

    if (b) {

        result += b;
    }

    if (c) {

        result += c;
    }

    return result;
}

console.log(sum(1, 2, 3));

console.log(sum(1, 2));

console.log(sum(1));

    </code>
  </pre>

  <span>
  <h2>4. TYPE VS INTERFACE</h2>

  <p>
    On retrouve sur la documentation de TypeScript des exemples qui alterne entre les types et les interfaces. 
    Même si dans beaucoup de cas les 2 peuvent être utilisés de manière interchangeable, 
    il y a des différences notables entre les deuxe.</p>
  <h3>4.1. Un type peut utiliser un type primaire</h3>
  
  <p>On peut avoir un type qui utilise des types de bases et des unions types.</p>

  <pre>
      <code>
        type DateString = string
        type Id = string | number
      </code>
  </pre>

  <p>Ce n'est pas possible avec les interfaces, qui ne peuvent décrire que des objets.</p>

  <h3>4.2. Une interface peut être fusionnée</h3>

  <p>Une interface peut être définie plusieurs fois et les déclarations seront alors fusionnées.</p>

</span>
<span>
  <h2></h2>
  <pre>
    <code>
      interface Point {
      x: number
      }

      interface Point {
          y: number
      }
      // Point aura maintenant la forme {x:number, y:number}
    </code>
  </pre>
  <p>C'est impossible avec les types, qui eux ne peuvent pas être modifiés une fois déclarés.</p>
</span>

<span>
  <h2></h2>
  <h3>4.3. Une interface peut être implémentée par une classe</h3>
  <p>
  Lorsque l'on déclare une classe, il est possible d'indiquer que l'on implémente une interface de manière explicite.
  </p>
  <pre>
    <code>
      class TwoDimensionalPoint implements Point {
          constructor (private x: number, private y: number) {}
      }
    </code>
  </pre>
</span>
<span>
  <h2></h2>
  <p></p>
  <pre>
    <code>
      

    </code>
  </pre>
</span>
<span>
  <h2></h2>
  <p></p>
  <pre>
    <code>
      

    </code>
  </pre>
</span>
<span>
  <h2></h2>
  <p></p>
  <pre>
    <code>
      

    </code>
  </pre>
</span>

/**************************************************
***************** exemples ************************
***************************************************/
<span>
  <h2></h2>
  <p></p>
  <pre>
    <code>
      const app = document.querySelector<HTMLDivElement>("#app")!;

const a: string = "Hey Oh";
const n: number = 41;
const b: boolean = true;
const d: null = null;

// array type
// ERROR // const arr: string[] = ["hug", "ugh", "hgu", 5]; // error msg: {"message": "Type 'number' is not assignable to type 'string'.",}
const arrStringNumber: any[] = ["hug", "ugh", "hgu", 5]; // it work but we lose all types benefices
const arr: string[] = ["hug", "ugh", "hgu"]; // OK, no error
// type an object, put the key in {} & we passed the value
const user: { firstname: string; lastname: string } = {
  firstname: "Neil",
  lastname: "Young",
};

const tech: { one: string; two: string } = {
  one: "TypeScript",
  two: "JavaScript",
};

    </code>
  </pre>
</span>

<span>
  <h2>Narrowing exemple</h2>
  <p>typeOf / conditions</p>
  <pre>
    <code>
      function printId(id: string | number) {
  if (typeof id === "number") {
    console.log((id + 18).toString()); // type number
  } else {
    console.log(id.toUpperCase()); // type string
  }
}

console.log(printId("Number Of ID: ")); // 'id === string' so 'id.toUpperCase()'
/**
 * a is a string (only 2 communs types)
 */
function communTypes(a: string | boolean, b: string | number) {
  if (a === b) {
    a; // understand type string, the only commun type
  }
}
/**
 * with the same function but boolean more type
 * a is a string (only 2 communs types)
 * completion on a understand type string or boolean
 */
function communTypesTwo(a: string | boolean, b: string | number | boolean) {
  if (a === b) {
    a; // understand type string or boolean
  }
}


    </code>
  </pre>
</span>


<span>
  <h2></h2>
  <p></p>
  <pre>
    <code>
      

    </code>
  </pre>
</span>
/**
 * Narrowing with inctanceOf
 * here we know that a is a Date type
 */
function narrowExemple(a: string | Date) {
  if (a instanceof Date) a; // know that a is a Date type
}

/**
 *
 * @param obj
 */

function getFavouriteNumber(): number {
  return 99;
}

//anonyme function : is syntax error in function name toUpperCase(), typescript  alert problem
// typage contrextuel, typescript

//utilise le typage de la fonction
//forEach pour deduire le type de s
const names = ["Gju", "Zry", "Uhtr"];
names.forEach(function (s) {
  console.log(s.toUpperCase());
});

names.forEach((t) => {
  console.log(t.toLowerCase());
});

// OBJET TYPE
function printCoord(pt: { x: number; y: number }) {
  console.log("geographic x point is " + pt.x);
  console.log("y is " + pt.y);
}
printCoord({ x: 88, y: 231 });
/**
 *
 * @param obj
 * undefined
 */
// specifie not undefined value0
function printName(obj: { first: string; last?: string }) {
  printName({ first: "TypeScript" });
  printName({ first: "TypeScript", last: "JavaScript" });

  if (obj.last !== undefined) {
    console.log("hey " + obj.last.toUpperCase());
  }
  console.log(obj.last?.toUpperCase());
}

<span>
  <h2></h2>
  <p></p>
  <pre>
    <code>
      

    </code>
  </pre>
</span>
/**
 * union type definition
 */
function printIdTwo(id: number | string) {
  console.log("your id is: " + id);
}

<span>
  <h2></h2>
  <p></p>
  <pre>
    <code>
      

    </code>
  </pre>
</span>
/**
 * in operator
 */
type Dog = { wouf: () => void };
type Cat = { miaou: () => void };

function sound(animal: Dog | Cat) {
  if ("wouf" in animal) {
    console.log(animal.wouf);
  } else {
    console.log(animal.miaou);
  }
}
// reiteration properties
type Fish = { swim: () => void };
type Bird = { fly: () => void };
type Human = { swim?: () => void; fly?: () => void };

function move(animal: Fish | Bird | Human) {
  if ("swim" in animal) {
    console.log(animal);
  } else {
    console.log(animal);
  }
}

<span>
  <h2></h2>
  <p></p>
  <pre>
    <code>
      

    </code>
  </pre>
</span>
/**
 * Alias & Generics
 */
type Artist = { firtname: string; lastname: string; age: number; bith?: Date };
type Id = string | number;

const artist: Artist = { firtname: "Neil", lastname: "Young", age: 82 };
console.log(artist); // w'? ' make optionnal 'birth' Date parameter, otherwise, we we got an error: 'Date type is missing'

/************* Why Use Gernerics, a really Great TS notion ? **************
this fist function get an issu exemple that Gerneric can fix 
**************************************************************************/
/**
 * In this first function we don't Use Gernerics, and a big problem is that
 * we lost the type: number became 'any'
 * @param arg
 */
function identityLost(arg: any): any {
  return arg;
}
const aaa = identityLost(56); // WARNING, the is a type problem:
//aaa variable is not number type but any type !!!
// ! Here wer lost number type on aa to any type, Here we lost the type: number became 'any'

<span>
  <h2></h2>
  <p></p>
  <pre>
    <code>
      

    </code>
  </pre>
</span>
/******************************************************************************************************
 * ************ Use Gernerics, a really Great TS notion, It make TypeScript so Great !!! **************
 *******************************************************************************************************/
/**
 * In this second function we Use Gernerics, and fix problem of sort lost type
 * We don't wana lost a type like number, and certainly no to'any' (! 'any) is the type to never use, expect we don't have the choice or ??? )
 * To fix this problem, we want that function give the same type in enter & on return
 * so, we can pass to the identity funtion a parameter type as <PascalCase>
 * TypeScript convention is to name this function parapeter with the PascalCase Convention
 * @param arg
 */
function identity<ArgType>(arg: ArgType): ArgType {
  return arg;
}
const aa = identity<number>(56); // To fix the loosing type we has in our first variable aaa, we want that function give the same type in enter & on return
// so, we can pass to the identity funtion a parameter type in <> & use PascalCase into

// literals types with gerenics
const aaLiteral = identity(99); // aaLiteral type is '99', it's a literal type

<span>
  <h2></h2>
  <p></p>
  <pre>
    <code>
      

    </code>
  </pre>
</span>
/********* OTHER EXEMPLE WE CAN USE GENERIC ************/
/**
 * 'first' function have 'any' in param, array & return type: 'any'
 * but 'any' type is not a good practice
 */
// function first(arg: any[]): any {}

/********* SO WE CAN USE GENERIC LIKE THIS *************/

/**
 * Generics on an array
 * 'first' function have 'Type' in param, array & return type: 'Type'
 */
function first<Type>(arg: Type[]): Type {
  return arg[0];
}
// Now, we can know the type
const bb = first(["aze", "eza", "zae", 99]); // here, variable type is 'string | numbre'
const bbb = first(["aze", "eza", "zae"]); // here, variable type is 'string'

<span>
  <h2></h2>
  <p></p>
  <pre>
    <code>
      

    </code>
  </pre>
</span>
/***************** GENERIC works on NATIVE FUNCTIONS ****************/
/**
 * on NATIVE FUNCTIONS: HTMLButtonElement
 * adding <HTMLButtonElement>
 * add 'HTMLButtonElement | null' type
 */
const compteur = document.querySelector<HTMLButtonElement>("#compteur");

/**
 * on NATIVE FUNCTIONS: ARRAY
 */
const arrayFirst = ["aze", "eza", "zae"];
const natFunArray: Array<string | number> = ["aze", "eza", "zae", 99];

/**
 * we can get a 'Type' with a 'generic type'
 */
type Identity<ArgType> = (arg: ArgType) => ArgType;

<span>
  <h2></h2>
  <p></p>
  <pre>
    <code>
      

    </code>
  </pre>
</span>
/*********** FORCING A TYPE WITH A CONSTRAINT **********/
// contraint is that length must be a numer of an array
/**
 * error on consoleSize function
 * return a 'any' type
 */
// function consoleSize(arg) {
//   console.log(arg.length);
//   return arg;
// }
/**
 * another error on consoleSize:
 * Property 'length' does not exist on type 'Type'
 */
// function consoleSize<Type>(arg: Type): Type {
//   console.log(arg.length);
//   return arg;
// }

<span>
  <h2></h2>
  <p></p>
  <pre>
    <code>
      

    </code>
  </pre>
</span>
/**
 * we can extends on an object with a key: lenght type: number
 * when we will used this function, we ll must give an array with his number
 * this is the constraint
 */
function consoleSize<Type extends { length: number }>(arg: Type): Type {
  console.log(arg.length);
  return arg;
}

// const azz = consoleSize(3); // ERROR : constraint not respected
const azz = consoleSize(["3", 2]); // OKAY, Constraint respected

/************* a type can depend from anoter one.*******************/
type P = keyof Artist; // 'P' take all types of 'Artist'
/************* a type can herite from anoter one.*******************/
type ArtisteName = Artist["firtname"];

/************final resum generic TS ***********/
const input = document.querySelector("input"); // TS allow to know that 'input' is HTMLInputElement type

<span>
  <h2></h2>
  <p></p>
  <pre>
    <code>
      

    </code>
  </pre>
</span>

<span>
  <h2>CLASS, Oriented Object</h2>
  
  <p></p>
  <ul>
    <li>exemple with reverse function that change array order that can cause troubles
    <li>we can add 'readonly' on a property</li>
    <li>this property can't be changed</li>
    <li>so, push or reverse are not allow</li>
    <li>we can do it on the return TYPE, or on all TYPES</li></li>
    <li></li> 
  </ul>
  <pre>
    <code>
      function reverse<T>(arr: readonly T[]): T[] {
  return arr.reverse();
}

  //  we can use a spead operator to create a new array

  // ON FUTURE JS VERSION, WE WILL PUT A # on PRIVATE instances

    </code>
  </pre>
</span>




function reverse<T>(arr: readonly T[]): T[] {
  return [...arr].reverse();
}
<span>
  <h3>Solution, Use a Sread Operator to work on a array copy !</h3>
  
  <p></p>
  <ul>
    <li>exemple with reverse function that change array order that can cause troubles !</li>

    <li>this property can't be changed</li>
    <li>so, push or reverse are not allow</li>
  </ul>
  <pre>
    <code>

      /**
       * with spead operator to worke into a new array
      */
      function reverse<T>(arr: readonly T[]): T[] {
        return [...arr].reverse();
      }
    </code>
  </pre>
</span>
<span>
  <h3>POO encapsulation</h3>
  <p></p>
  <pre>
    <code>
      class A {
  public x = 3;
  private a = 30; // see POO encapsulation
  #aa = 30; // in futur ECMASCRIPT, # is like private

  log() {
    console.log(this.a); // ok, accessible because inside Classe A
  }

  log2() {
    console.log(this.#aa); // ok, accessible because inside Classe A
  }
}

const xInstance = new A();
const aInstance = new A();

console.log(xInstance.x); // ok, accessible
// console.log(aInstance.a); // error no accessible outside class instance

aInstance.log(); // ok , private, we can accessible inside Classe

class B {
  protected y = 857; // only children can access
}

class C extends B {
  log() {
    console.log(this.y); // Ok can acces because a is protected
  }
}
    </code>
  </pre>
</span>


/**
 * ENCAPTULATION IS ONLY ON TS, NOT ON JS
 */

// console.log();

/**
 * Generate visibility by contructor with TYPE
 */
class D {
  constructor(public z: number) {}
}
const dInstance = new D(56);

console.log(dInstance.z); // ok, accessible with TYPE

/**
 * Generic Class & Contructor for TYPE
 *
 * this make reference on instance in POO
 */
class Collection<T> {
  constructor(private items: T[]) {}

  add(item: T): this {
    // Type return this, so the same istence is send
    this.items.push(item);
    return this; // ok, no error
  }
  first() {
    return this.items[0] || null; // reference on instance
  }
}

// create a new Collection, specifie comtent inside & initialise with good type
// const h = new Collection<number>([1, 2, 3, 4, 5, 6, 5, 4, 3, 2, 1, 0]);

// const h = new Collection([1, "2", 3, 4, 5, 6, 5, 4, 3, 2, 1, 0]);

const h = new Collection([1, "2", 3, 4, 5, 6, 5, 4, 3, 2, 1, 0]);
const i = h.add(300); // allways be a Collection type <number> but if I add a String in the new Collection array, type will change to TYPES: Collection<string | number>
const g = h.first();
// TS GIVE US THE TYPES OF VARIABLES LIKE
// const h: Collection<number>
// const g: number | null

/**
 * WE CAN CHANGE 'THIS' CONTEXT
 *
 * WARNING, IN FINAL COMPILE CODE, THIS CONTEXT CAN NO WORK
 *
 *CHANGE TO AN ARROW FUNCTION (see Subscriber2 function)
 */
class Subscriber {
  // put 'this' in function param
  on(this: HTMLInputElement, name: string, cb: Function) {
    // NOW every time I CALL 'THIS, TYPE ll reference to 'HTMLInputElement', GREAT !!!
    // this.
  }
}

class Subscriber2 {
  // CHANGE TO AN ARROW FUNCTION to KEEP CODE When it will be compile in JS
  on = (name: string, cb: Function) => {
    // NOW every time I CALL 'THIS, TYPE ll reference to 'HTMLInputElement', GREAT !!!
    // this.
  };
}
/************************************
 * *********************************
 * ********************************
 */
  `;
