# TypeScript Cheat Sheet

## Install

`npm install TypeScript`

- run :
  `npx tsc`

- run with a spec config :
  `npx tsc --project configs/my_tsconfig.json`

## Triple slash directives

- Reference built-in types
  /// <reference lib="es2016.array.include" />

- Reference other types
  /// <reference path="../my_types" />
  /// <reference types="jquery" />

- AMD
  /// <amd-module name="Name" />
  /// <amd-dependency path="app/foo" name="foo" />

## Compiler comments

- Don’t check this file
  // @ts-nocheck

- Check this file (JS)
  // @ts-check

- Ignore the next line
  // @ts-ignore

- Expect an error on the next line
  // @ts-expect-error

- Operators (TypeScript-specific and draft JavaScript)

```ts
?? (nullish coalescing)
function getValue(val?: number): number | 'nil' {
  // Will return 'nil' if `val` is falsey (including 0)
  // return val || 'nil';

  // Will only return 'nil' if `val` is null or undefined
  return val ?? 'nil';
}
```

- ?. (optional chaining)

```ts
function countCaps(value?: string) {
  // The `value` expression be undefined if `value` is null or
  // undefined, or if the `match` call doesn't find anything.
  return value?.match(/[A-Z]/g)?.length ?? 0;
}
```

- ! (null assertion)

```ts
let value: string | undefined;

// ... Code that we're sure will initialize `value` ...

// Assert that `value` is defined
console.log(`value is ${value!.length} characters long`);
```

- &&=

```ts
let a;
let b = 1;

// assign a value only if current value is truthy

a &&= "default"; // a is still undefined
b &&= 5; // b is now 5
```

## Basic types

- Untyped
  `any`
- A string
  `string`
- A number
  `number`
- A true / false value
  `boolean`
- A non-primitive value
  `object`

* Uninitialized value
  `undefined`
